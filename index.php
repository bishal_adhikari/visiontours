
<?php
require "connect.inc.php";

$advertisements="";
$query_run = mysqli_query($con,"SELECT * FROM tb_advertisement");
$ads_count= mysqli_num_rows($query_run);

if($ads_count>0){
  while ($row = mysqli_fetch_array($query_run)) {

	$ads_id=$row['id'];
	$ads_title= $row['ads_title'];
	$price_range=$row['price_range'];
	

	$advertisements.='<div class="grid_4">
	<div class="banner">
		<img src="images/ban_img2.jpg" alt="">
		<div class="label">
			<div class="title">'.$ads_title.'</div>
			<div class="price">FROM<span>$'.$price_range.'</span></div>
			<a href="#">LEARN MORE</a>
		</div>
	</div>
</div>';

// echo $advertisements;
} 
}
?>
<html lang="en">
<!-- <body style="background-image:url(images/body.jpg)"> -->
<link rel="stylesheet" type="text/css" href="css/socials.css">

	<?php
	include('header.php');	
	include('navigation.php');


	?>
	
<script>
			$(document).ready(function(){
			jQuery('#camera_wrap').camera({
				loader: false,
				pagination: false ,
				minHeight: '444',
				thumbnails: false,
				height: '48.375%',
				caption: true,
				navigation: true,
				fx: 'mosaic'
			});
			/*carousel*/
			var owl=$("#owl");
				owl.owlCarousel({
				items : 2, //10 items above 1000px browser width
				itemsDesktop : [995,2], //5 items between 1000px and 901px
				itemsDesktopSmall : [767, 2], // betweem 900px and 601px
				itemsTablet: [700, 2], //2 items between 600 and 0
				itemsMobile : [479, 1], // itemsMobile disabled - inherit from itemsTablet option
				navigation : true,
				pagination : false
				});
			$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
	<script src="js/owl.carousel.js"></script>
		<script src="js/camera.js"></script>
		<!--[if (gt IE 9)|!(IE)]><!-->
		<script src="js/jquery.mobile.customized.min.js"></script>
		<!--<![endif]-->
		<script src="booking/js/booking.js"></script>
	<body class="page1" id="top">
<!--==============================header=================================-->
<div class="sticky-container">
    <ul class="sticky">
        <li>
            <img src="images/facebook-circle.png" width="32" height="32">
            <p><a href="https://www.facebook.com/Mount-Vision-treks-and-expedition-1960454267509742/" target="_blank">Like Us on<br>Facebook</a></p>
        </li>
            <li>
            <img src="images/instagram-circle.png" width="32" height="32">
            <p><a href="https://www.instagram.com/mount_vision.treks/" target="_blank">Follow Us on<br>Instagram</a></p>
        </li>

        <li>
            <img src="images/twitter-circle.png" width="32" height="32">
            <p><a href="https://twitter.com/mountvisiontrek?lang=en" target="_blank">Follow Us on<br>Twitter</a></p>
        </li>
        <li>
            <img src="images/gplus-circle.png" width="32" height="32">
            <p><a href="https://plus.google.com/113748352347524637037" target="_blank">Follow Us on<br>Google+</a></p>
        </li>
        <li>
            <img src="images/linkedin-circle.png" width="32" height="32">
            <p><a href="https://www.linkedin.com/in/mount-vision-treks-1a4497155/" target="_blank">Follow Us on<br>LinkedIn</a></p>
        </li>

    </ul>
</div>

		<div class="slider_wrapper">
			<div id="camera_wrap" class="">
				<div data-src="images/slide.jpg">
					<div class="caption fadeIn">
						<h2>Annapurna</h2>
						<div class="price">
							FROM
							<span>$1000</span>
						</div>
						<a href="#">LEARN MORE</a>
					</div>
				</div>
				<div data-src="images/slide1.jpg">
					<div class="caption fadeIn">
						<h2>River Side</h2>
						<div class="price">
							FROM
							<span>$2000</span>
						</div>
						<a href="#">LEARN MORE</a>
					</div>
				</div>
				<div data-src="images/slide2.jpg">
					<div class="caption fadeIn">
						<h2>RARA LAKE</h2>
						<div class="price">
							FROM
							<span>$1600</span>
						</div>
						<a href="detailspage.php">LEARN MORE</a>
					</div>
				</div>
			</div>
		</div>
<!--==============================Content=================================-->
		<!-- <div class="content"><div class="ic">More Website Templates @ TemplateMonster.com - February 10, 2014!</div> -->
			<div class="container_12">
				<!-- <div class="grid_4">
					<div class="banner">
						<img src="images/ban_img1.jpg" alt="">
						<div class="label">
							<div class="title">Barcelona</div>
							<div class="price">FROM<span>$ 1000</span></div>
							<a href="#">LEARN MORE</a>
						</div>
					</div>
				</div> -->
				<?php echo $advertisements;?>
<!-- 			
				<div class="grid_4">
					<div class="banner">
						<img src="images/ban_img2.jpg" alt="">
						<div class="label">
							<div class="title">GOA</div>
							<div class="price">FROM<span>$ 1.500</span></div>
							<a href="#">LEARN MORE</a>
						</div>
					</div>
				</div>
				<div class="grid_4">
					<div class="banner">
						<img src="images/ban_img3.jpg" alt="">
						<div class="label">
							<div class="title">PARIS</div>
							<div class="price">FROM<span>$ 1.600</span></div>
							<a href="#">LEARN MORE</a>
						</div>
					</div>
				</div> -->


								
			     
			     	
					
					<!-- <div class="grid_5 prefix_1"> -->
					<div class="container">
                    <div class="row">
                    <div class="col-sm-4">
					<h3><b>Welcome</b></h3>
					<!-- <img src="images/page1_img1.jpg" alt="" class="img_inner fleft"> -->
					<div class="extra_wrapper">
					<p>Welcome to Destination Mt. Vision. Formally we are recognized as a leading specialists for trekking, tour and adventure activities operator in the Himalayan land of  course Nepal where we are primarily based.
    				</div>
					<div class="clear cl1"></div>
					 It is all familiar that Himalayas and its foothills is the best destination for trekking, expedition, peak climbing, adventure sports, pilgrimage tour, sightseeing tours, and many others.
					</p>
					 </div>
					 <div class="col-sm-6">
					 <h4>Clients’ Quotes</h4>
				    <blockquote class="bq1">
						<img src="images/page1_img2.jpg" alt="" class="img_inner noresize fleft">
						<div class="extra_wrapper">
							<p> I had a fabulous time trekking with Mt.Vision and became a memorable trip.
							Everything was brilliantly organized by Mt,Vision and our guide Suman was a superstar. Would recommend Poon Hill Trek to anyone. Stunning place. </p>
							<div class="alright">
								<div class="col1">Miranda Brown</div>
								<a href="review.php" class="btn">More</a>
							</div>
						</div>
					</blockquote>
				</div>
			</div>
		</div>
				<div class="grid_12">
					<h3 class="head1">Latest News</h3>
				</div>
				<div class="grid_4">
					<div class="block1">
						<time datetime="2014-01-01">31<span>Dec</span></time>
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="blog.php">Pokhara</a></div>
							Celebatre Brand New Year 2018 in Pokhara and Lake Side
						</div>
					</div>
				</div>
				<div class="grid_4">
					<div class="block1">
						<time datetime="2014-01-01">21<span>Jan</span></time>
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="blog.php">Lumbini</a></div>
							Explore the Birthplace of Lord Buddha and more about Buddhism .
						</div>
					</div>
				</div>
				<div class="grid_4">
					<div class="block1">
						<time datetime="2014-01-01">15<span>Feb</span></time>
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="blog.php">Mustang</a></div>
							This Winter season and playing with snow is insane stunning.
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			$(function (){
				$('#bookingForm').bookingForm({
					ownerEmail: '#'
				});
			})
			$(function() {
				$('#bookingForm input, #bookingForm textarea').placeholder();
			});
		</script>
	</body>
</html>
<?php
include('footer.php');
?>
