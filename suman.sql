-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2017 at 12:19 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suman`
--

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `review` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `name`, `review`) VALUES
(1, 'suman', 'lakh'),
(2, 'bishal', 'hello'),
(3, 'Bishal', 'kjfld');

-- --------------------------------------------------------

--
-- Table structure for table `tb_advertisement`
--

CREATE TABLE `tb_advertisement` (
  `id` int(5) NOT NULL,
  `ads_title` varchar(40) DEFAULT NULL,
  `price_range` int(11) DEFAULT NULL,
  `ads_image` varchar(30) NOT NULL,
  `ads_url` varchar(40) DEFAULT NULL,
  `addposition` varchar(30) NOT NULL,
  `ads_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_advertisement`
--

INSERT INTO `tb_advertisement` (`id`, `ads_title`, `price_range`, `ads_image`, `ads_url`, `addposition`, `ads_status`) VALUES
(22, 'France', 1500, 'flip.jpg', 'www.flipkart.com', 'top', 1),
(23, 'Goa', 2000, 'jabong2ad.jpg', 'www.jabong.com', 'sbr', 0),
(33, 'Barcelona', 2500, '8 01 2016 - 7.jpg', 'asdkhajdhkjad', 'top', 1),
(34, 'new country', NULL, '', NULL, '', NULL),
(35, 'new d', NULL, '', NULL, '', NULL),
(36, 'dfd', NULL, '', NULL, '', NULL),
(37, 'Nepal', NULL, '', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` int(15) NOT NULL,
  `category_name` varchar(40) DEFAULT NULL,
  `category_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`, `category_status`) VALUES
(1, 'international', 1),
(2, 'national', 1),
(3, 'politics', 0),
(4, 'sports', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_contact`
--

CREATE TABLE `tb_contact` (
  `id` int(20) NOT NULL,
  `fullname` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `message` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contact`
--

INSERT INTO `tb_contact` (`id`, `fullname`, `city`, `email`, `message`) VALUES
(1, 'aadsd', 'sdsdsdsd', 'dsf,sdfsdf', 'asdgasdjadgjagdjhad'),
(2, 'devraj khadaka', 'kathmandu', 'devraj', 'this is my name '),
(3, 'asdjh', 'w', 'q', 'adadasdfgdfgdgfg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_news`
--

CREATE TABLE `tb_news` (
  `news_id` int(15) NOT NULL,
  `category_id` int(15) NOT NULL,
  `news_title` varchar(40) DEFAULT NULL,
  `news_type` varchar(30) NOT NULL,
  `updatedate` varchar(20) NOT NULL,
  `news_description` text,
  `news_status` tinyint(1) DEFAULT NULL,
  `news_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_news`
--

INSERT INTO `tb_news` (`news_id`, `category_id`, `news_title`, `news_type`, `updatedate`, `news_description`, `news_status`, `news_image`) VALUES
(38, 4, 'sports', '0', '16-02-16', '<p>Gareth Bale is flying in RealMadrid this season having already scored more than 40 goals this season.He is in the best form of hs career.</p>\r\n', 1, '-310459106.jpg'),
(39, 5, 'What are we seeing', '1', '16-02-16', '<p>What are happening in Nepal . The kantipur host has been relieved of his duties as he was found with another woman at car at night at 3am.</p>\r\n', 1, '10420036_1523073304572969_7040686744624767664_n.jpg'),
(42, 1, 'obama', '0', '16-02-18', '<p>obama were discussing about the economic codition in Nepal. they are discussing about providing employment in nepal.</p>\r\n', 1, 'david.jpg'),
(44, 5, 'entertainment', '1', '02/19/2016', '<p>chienese people were having a rice party in which they eat the food o themselves with the food of others.The chinese ladies then were having the party of the last time.they were all drunk and have the chance to marry with other boys.The boys are very lucky ................</p>\r\n', 1, 'chinese.jpg'),
(45, 2, 'Lady Killed', '0', '16-02-22', '<p>This lady has been killed with the another man on the thapathali.Police are throughly investigating the happenings.</p>\r\n', 1, '1653600_735400053137109_334408760_n.jpg'),
(49, 1, 'asdas', '1', '16-03-16', 'write your description', 1, '1620566_519752788143099_1395786998_n.jpg'),
(50, 2, 'dassad', '0', '16-03-16', 'write your description', 1, '$T2eC16hHJFoE9nh6qS4YBRQ49GRk1g~~60_35.JPG'),
(52, 2, 'uiuiuii', '1', '16-03-17', 'write your description', 1, '4_classicsportscars_jaguare-type.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_slider`
--

CREATE TABLE `tb_slider` (
  `id` int(5) NOT NULL,
  `slider_image` varchar(40) DEFAULT NULL,
  `slider_caption` text,
  `slider_description` varchar(100) DEFAULT NULL,
  `slider_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slider`
--

INSERT INTO `tb_slider` (`id`, `slider_image`, `slider_caption`, `slider_description`, `slider_status`) VALUES
(57, '-1109873147.jpg', 'Real Madrid', 'Real Madrid have already surprassed their preious tally of 1000 goals in a season .This all goes to ', 1),
(58, '10150743_287256154758127_690462195_n.jpg', 'Real Madrid fans', 'Real Madrid fans are really bery beautiful', 1),
(59, 'WOW (11).jpg', 'warcraft new Game released', 'This warcraft is the best game ever made in the world.', 1),
(61, '2castle_england.jpg', 'my tower', 'jaskjdhakjhdkjahdkjahdkj', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_social`
--

CREATE TABLE `tb_social` (
  `id` int(14) NOT NULL,
  `socialimage` varchar(20) NOT NULL,
  `socialtitle` varchar(20) NOT NULL,
  `url` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_social`
--

INSERT INTO `tb_social` (`id`, `socialimage`, `socialtitle`, `url`, `status`) VALUES
(7, 'facebook.gif', 'facebook', 'www.facebook.com', 1),
(8, 'twitter.gif', 'twitter', 'www.twitter.com', 1),
(9, 'Google-Plus.png', 'google', 'www.googleplus.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_subscribe`
--

CREATE TABLE `tb_subscribe` (
  `id` int(15) NOT NULL,
  `subscriberfullname` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_subscribe`
--

INSERT INTO `tb_subscribe` (`id`, `subscriberfullname`, `email`) VALUES
(7, 'DevRaj Khadka', 'devrajkhadka39@yahoo'),
(8, 'RameshGiri', 'Rameshgiri@yahoo.com'),
(9, 'sakdjhaskjd', 'asjkdhjkashda'),
(10, 'asdhjkasd', 'ashdkjashd');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL,
  `fullname` varchar(40) DEFAULT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `address` varchar(40) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `fullname`, `username`, `password`, `email`, `address`, `status`) VALUES
(12, 'devraj', 'dev', 'devaaaaaaaaaaaa', 'shyam@gmal.com', 'chhauni', 0),
(13, 'asgdajhsdgjhasd', 'dev', 'sdasdasd', 'dasdasdasd', 'asdasddsa', 0),
(14, 'aaaaa', 'dev', 'dev', 'asd', 'sad', 0),
(15, 'wwwwwwwww', 'dev', 'dev', 'dasdasdasd', 'asdh', 1),
(16, 'rrrr', 'dev', 'dev', 'asdgahjdsgjhasd', 'asjkdhasjdhaskjd', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_advertisement`
--
ALTER TABLE `tb_advertisement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_news`
--
ALTER TABLE `tb_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `tb_slider`
--
ALTER TABLE `tb_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_social`
--
ALTER TABLE `tb_social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_subscribe`
--
ALTER TABLE `tb_subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_advertisement`
--
ALTER TABLE `tb_advertisement`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `category_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_contact`
--
ALTER TABLE `tb_contact`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `news_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `tb_slider`
--
ALTER TABLE `tb_slider`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `tb_social`
--
ALTER TABLE `tb_social`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_subscribe`
--
ALTER TABLE `tb_subscribe`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
