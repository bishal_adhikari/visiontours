<!DOCTYPE html>
<html lang="en">

	<head>
		<title>About Us:Our Company,our Team,About Nepal,why with us ?</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no" />
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		
	<script>
		$(document).ready(function(){
			$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		</head>
	<body>
	<?php
			include('navigation.php');
			?>
			
		

<!--==============================Content=================================-->
			<div class="container">
				<div class="row">
					<h3><b><u>About Us !</u></b></h3>
					<div class="block2">
						<img src="images/page3_img1.jpg" alt="" class="img_inner fleft">
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="#">About Nepal</a></div>
							<p>Cras facilisis, nulla vel viverra auctor, leo gna sodales felis, quis malesuada nibh odio ut velit. Proin pharetra luctus diam, a celerisque eros convallis accumsan. </p>Maecenas vehicula egestas venenatis. Duis massa elit, auctor non pellentesque vel
							<br>
							<a href="#" class="link1">LEARN MORE</a>
						</div>
					</div>
					<hr style="height:1px" color="black">
					<div class="block2">
						<img src="images/page3_img2.jpg" alt="" class="img_inner fleft">
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="#">The Company</a></div>
							<p>Cras facilisis, nulla vel viverra auctor, leo gna sodales felis, quis malesuada nibh odio ut velit. Proin pharetra luctus diam, a celerisque eros convallis accumsan. </p>Maecenas vehicula egestas venenatis. Duis massa elit, auctor non pellentesque vel
							<br>
							<a href="#" class="link1">LEARN MORE</a>
						</div>
					</div>
					<hr style="height:1px" color="black">
					<div class="block2">
						<img src="images/page3_img3.jpg" alt="" class="img_inner fleft">
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="#">Legal Documents</a></div>
							<p>Cras facilisis, nulla vel viverra auctor, leo gna sodales felis, quis malesuada nibh odio ut velit. Proin pharetra luctus diam, a celerisque eros convallis accumsan. </p>Maecenas vehicula egestas venenatis. Duis massa elit, auctor non pellentesque vel
							<br>
							<a href="#" class="link1">LEARN MORE</a>
						</div>
					</div>
					<hr style="height:1px" color="black">
					<div class="block2">
						<img src="images/page3_img3.jpg" alt="" class="img_inner fleft">
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="#">Why With Us?</a></div>
							<p>Cras facilisis, nulla vel viverra auctor, leo gna sodales felis, quis malesuada nibh odio ut velit. Proin pharetra luctus diam, a celerisque eros convallis accumsan. </p>Maecenas vehicula egestas venenatis. Duis massa elit, auctor non pellentesque vel
							<br>
							<a href="#" class="link1">LEARN MORE</a>
						</div>
					</div>
					<hr style="height:1px" color="black">
					<div class="block2">
						<img src="images/page3_img3.jpg" alt="" class="img_inner fleft">
						<div class="extra_wrapper">
							<div class="text1 col1"><a href="#">Our Team</a></div>
							<p>Cras facilisis, nulla vel viverra auctor, leo gna sodales felis, quis malesuada nibh odio ut velit. Proin pharetra luctus diam, a celerisque eros convallis accumsan. </p>Maecenas vehicula egestas venenatis. Duis massa elit, auctor non pellentesque vel
							<br>
							<a href="#" class="link1">LEARN MORE</a>
						</div>
					</div>
					
				</div>

			</div>
		</div>
<hr style="height:1px" color="black">
		<script>
		$(function (){
			$('#bookingForm').bookingForm({
				ownerEmail: '#'
			});
		})
		</script>
	</body>
</html>
<?php
include('footer.php');
?>