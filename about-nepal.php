<?php
 $current_url = basename($_SERVER['PHP_SELF']);
?>
 

<head>
		<title>About</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no" />
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="booking/css/booking.css">
		<link rel="stylesheet" href="css/camera.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		
		
		<!--[if lt IE 8]>
		// <div style=' clear: both; text-align:center; position: relative;'>
		// 	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
		// 		<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		// 	</a>
		// </div>
		// <![endif]-->
		 <!--[if lt IE 9]>
		// <script src="js/html5shiv.js"></script>
		// <link rel="stylesheet" media="screen" href="css/ie.css">
		// <![endif]-->
	</head>

<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/dropdown.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<header>
			<div class="container_12" >
				<div class="grid_12">
					<div class="menu_block">
						<nav class="horizontal-nav full-width horizontalNav-notprocessed">
							<ul class="sf-menu">						
								<li <?php echo ($current_url == "index.php") ? 'class="current"' : ''?>><a href="index.php">home</a></li>	
								<li <?php echo ($current_url == "aboutus.php") ? 'class="current"' : ''?>><a href="aboutus.php">about us</a>
									<ul>
										<div class="dropdown">
   										<div class="dropdown-content">
										<li><a href="#">about nepal</a></li>
										<li><a href="#">the company</a></li>
										<li><a href="#">legal document</a></li>
										<li><a href="#">why with us</a></li>
										<li><a href="#">our team</a></li>
									    </div>
									</div>
									</ul>
								</li>

								<li <?php echo ($current_url == "all-tours.php") ? 'class="current"' : ''?>><a href="hot-tours.php">all tours</a>
									<ul> 
									<div class="dropdown">
   									<div class="dropdown-content">                             
                                    <li><a href="#">trekking</a>
                                    <li><a href="#">Trek & Travels</a>
                                    <ul>
									<li><a href="#">Village trekking</a></li>
                                   	<li><a href="#">City Sight-seeing</a></li>
                                   	<li><a href="#">Cultural & Pilgrims Visit</a></li>
                                   </ul>
                                   </li>
                                   <li><a href="#">adventure</a>
                                   <ul>
                                   	<li><a href="#">jungle safari</a></li>
                                   	<li><a href="#">village tours</a></li>
                                   	<li><a href="#">rafting</a></li>
                                   	<li><a href="#">peak climbing</a></li>
                                   	<li><a href="#">mountain bike</a></li>
                               </div>
                               </div>
                               </ul>
                               </li>
								<li <?php echo ($current_url == "blog.php") ? 'class="current"' : ''?>><a href="blog.php">BLOG/News</a></li>
								<li <?php echo ($current_url == "contact.php") ? 'class="current"' : ''?>><a href="contact.php">CONTACTS</a></li>								
								<li <?php echo ($current_url == "review.php") ? 'class="current"' : ''?>><a href="review.php">Review</a></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="grid_12">
					<h1>
						<a href="index.php">

							<!-- <img src="images/logo.jpg" alt="Your Happy Family"> -->

							<!-- <img src="images/logo.png" alt="Your Happy Family"> -->

						</a>
					</h1>
				</div>
			</div>
		</header>
		<?php
		include('footer.php');

		?>