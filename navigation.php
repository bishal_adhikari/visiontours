<?php
 $current_url = basename($_SERVER['PHP_SELF']);

//  require 'connect.inc.php';
//  require 'core.inc.php';
 

?>
<!-- <link rel="stylesheet" type="text/css" href="css/dropdown.css"> -->


<header>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.2.1/bootstrap-hover-dropdown.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.2.1/bootstrap-hover-dropdown.min.js"></script>

<link rel="stylesheet" href="css/style.css">

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="#">NavBar</a> -->
        </div>
        <div class="collapse navbar-collapse">
            <!-- <ul class="nav navbar-nav navbar-right">
                <li><a href="https://github.com/fontenele/bootstrap-navbar-dropdowns" target="_blank">GitHub Project</a></li>
            </ul> -->
            <ul class="nav navbar-nav">
                <li <?php echo ($current_url == "index.php") ? 'class="active"' : ''?>><a href="index.php">Home</a></li>
                <li <?php echo ($current_url == "aboutus.php") ? 'class="active"' : ''?>>
                    <a href="aboutus.php" class="dropdown-toggle" data-toggle="dropdown">About us <b class="caret"></b></a>
                    <ul class="dropdown-menu multi-level">
						<li><a href="#">about nepal</a></li>
						<li><a href="#">the company</a></li>
						<li><a href="#">legal document</a></li>
						<li><a href="#">why with us</a></li>
						<li><a href="#">our team</a></li>
                        
                    </ul>
                </li>
                <li <?php echo ($current_url == "hot-tours.php") ? 'class="active"' : ''?>>
                    <a href="hot-tours.php" class="dropdown-toggle" data-toggle="dropdown">All TOURS <b class="caret"></b></a>
                    <ul class="dropdown-menu multi-level">
					    
					<li class="dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Trek And Travels</a>
                            <ul class="dropdown-menu">
								<li><a  href="#">Village trekking</a></li>
								<li><a href="#">City Sight-seeing</a></li>
								<li><a href="#">Cultural & Pilgrims Visit</a></li>
                                
                            </ul>
                        </li>
                
                        <li class="dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Adventure</a>
                            <ul class="dropdown-menu">
								<li ><a href="#">jungle safari</a></li>
								<li><a href="#">village tours</a></li>
								<li><a href="#">rafting</a></li>
								<li><a href="#">peak climbing</a></li>
								<li><a href="#">mountain bike</a></li>
                                
                            </ul>
                        </li>
                    </ul>
                </li>

				<li <?php echo ($current_url == "blog.php") ? 'class="active"' : ''?>><a href="blog.php">BLOG/News</a></li>
				<li <?php echo ($current_url == "contact.php") ? 'class="active"' : ''?>><a href="contact.php">CONTACTS</a></li>								
				<li <?php echo ($current_url == "review.php") ? 'class="active"' : ''?>><a href="review.php">Review</a></li>
								
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
			
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
</header>
