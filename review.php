<!DOCTYPE html>
<?php
require 'connect.inc.php';
$reviews="";
$query_run = mysqli_query($con,"SELECT * FROM review");

$review_count= mysqli_num_rows($query_run);

if($review_count>0){
  while ($row = mysqli_fetch_array($query_run)) {

	
	$review_id=$row['id'];
	$name= $row['name'];
	$review_body=$row['review'];
	

	$reviews.='<h5>'.$name.'</h5><br><p>'.$review_body.'</p>';
	;


} 

}
?>
<html lang="en">
	<head>
		<title>Contacts</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no" />
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="css/form.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/TMForm.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script>
		$(document).ready(function(){
			$().UItoTop({ easingType: 'easeOutQuart' });
			});
		</script>
		<!--[if lt IE 8]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
				<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
	<body>
	<?php
			include('navigation.php');
			?>
<!--==============================header=================================-->
		<!-- <header>
			<div class="container_12">
				<div class="grid_12">
					<div class="menu_block">
						<nav class="horizontal-nav full-width horizontalNav-notprocessed">
							<ul class="sf-menu">
								<li><a href="index.html">ABOUT</a></li>
								<li><a href="index-1.html">HOT TOURS</a></li>
								<li><a href="index-2.html">SPECIAL OFFERS</a></li>
								<li><a href="index-3.html">BLOG</a></li>
								<li class="current"><a href="index-4.html">CONTACTS</a></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="grid_12">
					<h1>
						<a href="index.html">
							<img src="images/logo.png" alt="Your Happy Family">
						</a>
					</h1>
				</div>
			</div>
		</header> -->
<!--==============================Content=================================-->
		<div class="content"><div class="ic">More Website Templates @ TemplateMonster.com - February 10, 2014!</div>
			<div class="container_12">
				<div class="grid_12">
					<h3>Reviews</h3>
					<div class="row">
						<div class="col-md-6">
						<form id ="review" action="post-review.php" method="POST">
						<label class="form-label">Name</label>

					<input type="text" class="form-control m-t-10" name="name" id="name"/>
					<label class="form-label">Review</label>
					
					<input type="text-area" class="form-control m-t-10" name="review" id="review"/>
					<button type="submit" class="btn btn-info pull-right m-t-10" > Post</button>
					</form>
						</div>
						<div class="col-md-6">
							Your review appears here
							<?php
							echo "$reviews";
							?>
						</div>
						
						
					</div>				
				
				</div>
			
			</div>
		</div>
<!--==============================footer=================================-->
		
		<script>
		$(function (){
			$('#bookingForm').bookingForm({
				ownerEmail: '#'
			});
		})
		</script>
	</body>
</html>
<?php
include('footer.php');
?>