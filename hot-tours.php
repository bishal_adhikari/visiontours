<!DOCTYPE html>
<html lang="en">
	<?php include('header.php');?>
	<body>
			<?php
			include('navigation.php');
			?>
<!--==============================header=================================-->

		<!-- <header>
			<div class="container_12">
				<div class="grid_12">
					<div class="menu_block">
						<nav class="horizontal-nav full-width horizontalNav-notprocessed">
							<ul class="sf-menu">
								<li><a href="index.html">ABOUT</a></li>
								<li class="current"><a href="index-1.html">HOT TOURS</a></li>
								<li><a href="index-2.html">SPECIAL OFFERS</a></li>
								<li><a href="index-3.html">BLOG</a></li>
								<li><a href="index-4.html">CONTACTS</a></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="grid_12">
					<h1>
						<a href="index.html">
							<img src="images/logo.png" alt="Your Happy Family">
						</a>
					</h1>
				</div>
			</div>
		</header> -->
<!--==============================Content=================================-->
		<div class="content"><div class="ic">More Website Templates @ TemplateMonster.com - February 10, 2014!</div>
			<div class="container_12">
				<div class="banners">
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img1.jpg" alt="">
							<div class="label">
								<div class="title">NEW ZEALAND</div>
								<div class="price">from<span>$ 1.200</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img2.jpg" alt="">
							<div class="label">
								<div class="title">GOA</div>
								<div class="price">from<span>$ 1.500</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img3.jpg" alt="">
							<div class="label">
								<div class="title">FRANCE</div>
								<div class="price">from<span>$ 1.600</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img4.jpg" alt="">
							<div class="label">
								<div class="title">CANADA</div>
								<div class="price">from<span>$ 2000</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img5.jpg" alt="">
							<div class="label">
								<div class="title">TURKEY</div>
								<div class="price">from<span>$ 1.500</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img6.jpg" alt="">
							<div class="label">
								<div class="title">EGYPT</div>
								<div class="price">from<span>$ 1.500</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img7.jpg" alt="">
							<div class="label">
								<div class="title">JAPAN</div>
								<div class="price">from<span>$ 1000</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img8.jpg" alt="">
							<div class="label">
								<div class="title">BRAZIL</div>
								<div class="price">from<span>$ 1.700</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
					<div class="grid_4">
						<div class="banner">
							<img src="images/page2_img8.jpg" alt="">
							<div class="label">
								<div class="title">Spain</div>
								<div class="price">from<span>$ 1.700</span></div>
								<a href="#">LEARN MORE</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
		$(function (){
			$('#bookingForm').bookingForm({
				ownerEmail: '#'
			});
		})
		</script>
	</body>
</html>
<script>
		$(document).ready(function(){
			$().UItoTop({ easingType: 'easeOutQuart' });
		});
		</script>
<hr>
<?php
include('footer.php');
?>